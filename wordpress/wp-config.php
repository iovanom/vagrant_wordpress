<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=0pF8[3j$nRvif@Zn@te#z{:p)Ztg(Y0]FCcPA{AR<zc7oI<lRw)sgTF3(7_:^oU');
define('SECURE_AUTH_KEY',  'TsqF({Z|VC{E[NBF8+PaL2F&CYwYU vau#esy322wJ*c:a_&KYmmtfYZNBh?yCsg');
define('LOGGED_IN_KEY',    'nG{HBL!=~gf7#`yST:NFFtgKts`UYK?EYL_V]3|Z</^d^Dw<vb1[X;`]~[<]xFpq');
define('NONCE_KEY',        '@CZSW *=A. GJH!oVkkT%gqTyC^,ZmElQ>l.Lw|&THLPO4Cofn6_~hsH!n}t{<^4');
define('AUTH_SALT',        'TdRm8X7YIFg)KgE5eeD3hR}psR*|3=86YV#p%P+#er]lVy/1dSR |7|3r9k%!N&!');
define('SECURE_AUTH_SALT', 'bg=ee`qvFg`}c~CFzdqM/oQ0WS)D{x$*Dp1h]B4rtW_~C_lc*3Qv|uS=4[P4FL,n');
define('LOGGED_IN_SALT',   'QXN]x$cN)PJ2xXv8]?7ud(=r+IL|g)C]CA6,2Up/8gLMBJ}A%GsBaC0x#GvG|R<S');
define('NONCE_SALT',       '7M`Nb.QI!E}4IVV^; A_/aFz_tsO?=,eXJjt4e5J;WO>6KpCdidMtOAA7~;+E}_:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
