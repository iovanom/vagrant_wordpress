import pika
import options
import os
import json
import subprocess as sp
from db_context import DbContext

def init_connection():

    conn = pika.BlockingConnection(pika.ConnectionParameters(options.RABBITMQ_HOST))
    channel = conn.channel()
    channel.queue_declare(queue=options.JOBS_QUEUE, durable=True)
    channel.basic_consume(get_job, queue=options.JOBS_QUEUE)
    channel.start_consuming()
    
def get_job(ch, method, properties, body):
    worker = VptfWorker(json.loads(body))
    ch.basic_ack(delivery_tag=method.delivery_tag)


class VptfWorker(object):

    IMAGES_COUNT = 15 

    def __init__(self, params):
        self.params = params
        self._parse_params()
        self._thumbnail_generate()
        if self.source['ext'] != 'mp4':
            self._transcode_video()
        self._update_status()


    def _parse_params(self):

        self.source = self.params['source']
        self.source['filename'] = os.path.basename(self.source['file'])
        self.source['dirname'] = os.path.dirname(self.source['file']) + os.sep
        self.source['name'] = '.'.join(self.source['filename'].split('.')[:-1])
        self.source['ext'] = self.source['filename'].split('.')[-1]
        self.source['reldir'] = self.source['dirname'][self.source['dirname'].find('wp-content') - 1:]
        self.post_id = self.params['post_id']

    def _thumbnail_generate(self):
        command = [ options.FFMPEG_BIN,
                '-i', self.source['file'],
                '-r', '0.1', '-ss', '00:00:30.000',
		'-vf', 'scale=600:340',
                '-vframes', str(self.IMAGES_COUNT),
                self.source['dirname'] + self.source['name'] + '-%d.png'
                ]

        pipe = sp.Popen(command)
        pipe.wait()
        if pipe.returncode == 0:
            self.status = 'ok'
            self._insert_thumbnails()
        else:
            self.status = 'fail'

    def _insert_thumbnails(self):
        sql = "INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES ( %s, 'thumbs', %s)"
        value = json.dumps([ self.source['reldir'] + self.source['name'] + '-' + str(n) + '.png' for n in range(1, self.IMAGES_COUNT + 1)])
        with DbContext() as c:
            c.execute(sql, (self.post_id, value))

    def _transcode_video(self):
        new_file = self.source['dirname'] + self.source['name'] + '.mp4'
        command = [ options.FFMPEG_BIN,
                '-i', self.source['file'],
                '-codec:v', 'libx264',
                '-codec:a', 'aac', '-strict', '-2',
                new_file
                ]
        pipe = sp.Popen(command, bufsize=10**8, stderr=sp.PIPE)
        pipe.wait()
        if pipe.returncode == 0:
            self.status = 'ok'
            self._update_video()
        else:
            self.status = 'fail'
            with open('./logs/log.txt', 'a') as f:
                f.write(pipe.communicate()[1] + '\n')

    def _update_video(self):
        new_meta_attached_file = self.source['reldir'][self.source['reldir']\
                .find('uploads') + len('uploads') + 1:] + self.source['name'] + '.mp4'

        update_meta_attachment_sql = ("UPDATE wp_postmeta SET meta_value = '{}'"
                            " WHERE post_id={} AND meta_key = '_wp_attached_file'"
                            .format(new_meta_attached_file, self.source['id']))

        new_guid = options.BASE_URL + self.source['reldir'] + self.source['name'] + '.mp4'
        update_guid_sql = ("UPDATE wp_posts SET guid = '{}' where ID = {}"
                        .format(new_guid, self.source['id']))


        with DbContext() as c:
            c.execute(update_meta_attachment_sql)
            c.execute(update_guid_sql)
            
    def _update_status(self):
        update_status_sql = ("UPDATE wp_postmeta SET meta_value = '{}'" 
                " WHERE post_id={} AND meta_key = 'vptf_status'"
                    .format(self.status, self.post_id))
        
        with DbContext() as c:
            c.execute(update_status_sql)


