import MySQLdb as mysql
import options

class DbContext(object):

    def __enter__(self):
        self.conn = mysql.connect(
                user=options.DB_USER,
                passwd=options.DB_PASSWORD,
                host=options.DB_HOST,
                db=options.DB_DATABASE)
        return self.conn.cursor()

    def __exit__(self, type, value, trace):
        self.conn.commit()
        self.conn.close()
