RABBITMQ_HOST = 'localhost'
JOBS_QUEUE = 'vptf_job'

FFMPEG_BIN = '/usr/bin/ffmpeg'

DB_USER = 'root'
DB_HOST = 'localhost'
DB_PASSWORD = 'root'
DB_DATABASE = 'test'

BASE_URL = 'http://test-wordpress.loc'
