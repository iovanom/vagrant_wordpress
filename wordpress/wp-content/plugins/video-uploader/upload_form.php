<div class="alert" id="alert" style="display:none"></div>
<?php
if( isset( $GLOBALS['vptf_upload_error'] ) ):
?>
<div class="alert alert-danger alert-dismissible fade in" id="alert">
    <?= $GLOBALS['vptf_upload_error']?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif;?>

<div id="upload-form" style="margin: 5px">
    <form id="upload-video">
        <div class="form-group">
            <label for="file">Select Video</label>
            <input type="hidden" name="name">
            <input type="file" name="file" id="file" />
        </div>
        <button class="btn btn-success" id="upload" name="upload">Upload Video</button>
    </form>
    <div class="progress" id="progress-bar">
      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
      </div>
    </div>
</div>

<div id="add-video" style="display:none">

    <form id="add-video" method="post">
        <div class="row">
            <div class="col-md-8">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" id="title" type="text" required="true" name="vptf_title">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="vptf_description" cols="30" rows="3"></textarea>
                    </div>
                    <input id="vptf_video_info" name="vptf_video_info" type="hidden">
                    <input name="vptf_upload" type="hidden" value="true">
                    <button id="add-btn" class="btn btn-success">Add Video</button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="category">Category</label>
                    <select class="form-control" id="category" name="vptf_category">

<?php foreach( get_categories( array( 'hide_empty'=>false ) ) as $category ):?>
                        <option value="<?= $category->cat_ID ?>"><?= $category->name?></option>
<?php endforeach; ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <div class="input-group">
                       <div class="input-group-addon">$</div>
                        <select id="price" class="form-control" name="vptf_price">
                            <option value="0" selected="selected">Free</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
jQuery( document ).ready(function() {

    var progress = jQuery('#progress-bar');
    var uplButton = jQuery('#upload');
    
    uplButton.prop('disabled', true);
    progress.hide();

    jQuery('#file').val("").change(function() {
        uplButton.prop('disabled', false);
        jQuery('#alert').hide();
        progress.hide();
    });

    jQuery('#upload-video').submit(function(e) {
        e.preventDefault();
        upload();
    });

    function upload() {

        var formData = new FormData();
        var response;
        var fileInputElement = document.getElementById("file");
        var xhr = new XMLHttpRequest();

        progress.show();

        formData.append("action", "upload-attachment");

        formData.append("async-upload", fileInputElement.files[0]);
        formData.append("name", fileInputElement.files[0].name);
        
        <?php $my_nonce = wp_create_nonce('media-form'); ?>
        formData.append("_wpnonce", "<?php echo $my_nonce; ?>");

        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4 && xhr.status == 200){
                progress.hide();
                response = JSON.parse(xhr.responseText);
            }

            if( xhr.readyState == 4 && (!response || !response.success) ) {
                jQuery('#alert').addClass('alert-danger').show()
                    .html('<?php echo __("ERROR! File not uploaded"); ?>');
            } else if( xhr.readyState == 4) {
                jQuery('#upload-form').hide(); 
                jQuery('#add-video').show();
                jQuery('#vptf_video_info').val(response.data.id);
            }
                
        }

        xhr.upload.addEventListener("progress", function(e) {
            var progressProc = Math.ceil(e.loaded / e.total) * 100;
            var progBar = progress.find('.progress-bar');
            progBar.css('width', progressProc + '%').attr('aria-valuenow', progressProc);
        }, false);

        xhr.open("POST", "/wp-admin/async-upload.php", true);
        xhr.send(formData);
    }
   
});


</script>
