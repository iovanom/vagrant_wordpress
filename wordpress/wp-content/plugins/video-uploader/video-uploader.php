<?php
/**
 * @package video uploader
 * @version 1.0
 */
/*
Plugin Name: Video Uploader 
Description: This plugin uplaod the video
Author: Ivan Majeru
Version: 1.0
Author URI: ivanmajeru@gmail.com
*/


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );  
require_once __DIR__ . '/vendor/autoload.php';

function vptf_upload_video() {
    
    if( isset ( $_POST['vptf_upload'] ) ) {
        
        if( isset ( $_POST['vptf_title'] ) and isset ( $_POST['vptf_video_info'] ) ) {
            $video_file = get_attached_file( intval($_POST['vptf_video_info']) );
            $video_info = wp_get_attachment_metadata( intval($_POST['vptf_video_info']) );

            if(isset($_POST['vptf_price'])){
                $video_price = intval(sanitize_text_field( $_POST['vptf_price']));
            } else {
                $video_price = 0;
            }

            $new_video = array(
                'post_title' => wp_strip_all_tags( $_POST['vptf_title'] ),
                'post_content' => isset( $_POST['vptf_description'] ) ? $_POST['vptf_description'] : __('No description'),
                'post_category' => array( intval( sanitize_text_field( $_POST['vptf_category'] ) ) ),
                'post_status' => 'publish',
                'meta_info' => array(
                    'video_id' => wp_strip_all_tags( $_POST['vptf_video_info'] )
                )
            );

            if( $post_ID = wp_insert_post( $new_video ) ) {
                wp_insert_attachment( array( 
                    'ID' => $new_video['meta_info']['video_id'],
                    'post_title' => $new_video['post_title'],
                    'post_parent' => $post_ID
                    )
                );
                add_post_meta($post_ID, 'vptf_status', 'processing', true);
                add_post_meta($post_ID, 'vptf_price', $video_price, true);
                vptf_add_job($video_file, $new_video['meta_info']['video_id'], $post_ID);
                wp_redirect( get_author_posts_url(wp_get_current_user()->ID));
                exit();
            } else {
                $GLOBALS['vptf_upload_error'] = __( 'Unknow error. Video not saved' );
            }
            
        } else {
            $GLOBALS['vptf_upload_error'] = __( 'The title and video file is required' );
        }

    }
} 

add_action( 'init', 'vptf_upload_video' );

//create short code for form 
function vptf_video_upload_shcode($atts) {
    if( is_user_logged_in() && current_user_can('publish_posts') ) {
        require( dirname( __FILE__ ) . '/upload_form.php');
    }
}
add_shortcode( 'vptf_upload_video', 'vptf_video_upload_shcode' );

function vptf_add_job($source, $attachment_id, $post_id) {

    $connection = new PhpAmqpLib\Connection\AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
    $channel = $connection->channel();
    $channel->queue_declare('vptf_job', false, true, false, false);
    $content = array( 
        "source" => array(
            "id" => $attachment_id,
            "file" => $source
        ),
        "post_id" => $post_id
    );
    $msg = new PhpAmqpLib\Message\AMQPMessage(json_encode($content));
    $channel->basic_publish($msg, '', 'vptf_job');
    $channel->close();
    $connection->close();
}


