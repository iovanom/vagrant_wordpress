<div class="panel panel-default" >
    <div class="panel-body">
        <ul class="nav nav-pills nav-stacked">
        <li class="<?php if(is_home()) echo 'active';?>"><a href="<?php bloginfo( 'url' ); ?>"><span class="glyphicon glyphicon-home"></span>  Recent Videos </a></li>
<?php if(is_user_logged_in()):?>
            <li ><a href="#">History</a></li>
            <li><a href="#">Liked Videos</a></li>
            <li><a href="#">Favorite Videos</a></li>
            <li><a href="#">Favorite Chanels</a></li>
<?php endif; ?>
            <li><a href="#" data-toggle="collapse" data-target="#cats">Categories</a>

                <ul class="list-unstyled collapse" id="cats">
<?php $cats = get_categories();?>
<?php //var_dump($cats);?>
<?php foreach($cats as $cat): ?>
    <li><a href="<?php echo get_category_link($cat->cat_ID); ?>"><?php echo $cat->name; ?></a>    <span class="badge"><?php echo $cat->count; ?></span></li>
<?php endforeach; ?>
                </ul>

            </li>

        </ul>
    </div>
</div>


