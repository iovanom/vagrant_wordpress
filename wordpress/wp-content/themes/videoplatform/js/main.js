(function($){

    $(document).ready(function(){

        //thumbnail slide
        $('.thumbnail img').on('mouseover', function(){

            var current_img = 0;
            var that = this;
            
            function changeImg(){
                $(that).attr('src', $(that).data('thumbs')[current_img]);

                if(current_img === $(that).data('thumbs').length){
                    current_img = 0;
                } else {
                    current_img++;
                }

                if(!window.thumb_interval){
                    window.thumb_interval = setInterval(function(){

                        changeImg();
                        
                    }, 500);
                }

            }

            changeImg();



        }).on('mouseout', function(){

            clearInterval(window.thumb_interval);
            window.thumb_interval = null;
            $(this).attr('src', $(this).data('thumbs')[0]);

        });

        console.log();
        //player
        var player = new MediaElementPlayer('#content-video video', {

            defaultVideoWidth: 840,
            videoWidth: $('#content-video').width()
        
        
        });
        


    });

})(jQuery);
