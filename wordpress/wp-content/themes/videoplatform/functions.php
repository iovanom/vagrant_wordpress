<?php 

//composer autoload
require __DIR__ . '/vendor/autoload.php';

if ( !function_exists( 'vptf_themesetup' ) ) {
    //theme suport
    function vptf_themesetup() {
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'post-formats', array(
            'video'
        ) );
    }
}

add_action( 'after_setup_theme', 'vptf_themesetup' );

//Add style and scripts
function vptf_enqueue_style() {
    $main_style = get_template_directory_uri() . '/style.css';
    $bootstrap = get_template_directory_uri() . '/css/bootstrap.css';
    $player = get_template_directory_uri() . '/js/player/build/mediaelementplayer.css';
    wp_enqueue_style( 'bootstrap', $bootstrap, false );
    wp_enqueue_style( 'player', $player, false);
    wp_enqueue_style( 'main_style', $main_style, array( 'bootstrap', 'player' ) );
    
}

function vptf_enqueue_scritp() {
    $npm_path = get_template_directory_uri() . '/node_modules';
    $jquery = $npm_path . '/jquery/dist/jquery.js';
    $bootstrap_js = $npm_path . '/bootstrap/dist/js/bootstrap.js';
    $player = get_template_directory_uri() . '/js/player/build/mediaelement-and-player.min.js';
    $main_js = get_template_directory_uri() . '/js/main.js';

    wp_enqueue_script( 'jquery', $jquery, false );
    wp_enqueue_script( 'bootstrap_js', $bootstrap_js, array( 'jquery' ) );
    wp_enqueue_script( 'player', $player, array( 'jquery' ) );
    wp_enqueue_script( 'main_js', $main_js, array( 'bootstrap_js' ), true );
}

add_action( 'wp_enqueue_scripts', 'vptf_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'vptf_enqueue_scritp' );

//add logo settings in theme customizer
function vptf_theme_customizer( $wp_customize ) {
    $wp_customize->add_section('vptf_logo_section', array(
        'title' => __('Logo'),
        'priority' => 30,
        'description' => 'Upload a logo image'
    ) );

    $wp_customize->add_setting( 'vptf_logo' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'vptf_logo', array(
        'label' => __('Logo'),
        'section' => 'vptf_logo_section',
        'settings' => 'vptf_logo',
    ) ) );
}
add_action( 'customize_register', 'vptf_theme_customizer' );

//redirect to home on logout
function vptf_redirect_logout() {
    wp_redirect( home_url() );
    exit();
}
add_action('wp_logout', 'vptf_redirect_logout');


//video viewed function

function vptf_video_viewed($post_id, $user_id, $increment=true) {
    $views = get_post_meta( $post_id, 'views', true ) ? get_post_meta( $post_id, 'views', true ) : 0;
    if($increment){
        $views++;
        update_post_meta( $post_id, 'views', $views, true);
        add_user_meta( $user_id, 'viewed_videos', $post_id);
    }
    return $views;
}

//paypal processing
function vptf_paypal_processing(){
    if(isset($_POST['paypal_processing'])){
        $post = get_post(intval($_POST['payed_post_id']));

        $video_price = get_post_meta($post->ID, 'vptf_price', true);

        if($video_price){
            //create api context
            require __DIR__ . '/paypal_bootstrap.php';

            $payer = new PayPal\Api\Payer();
            $payer->setPaymentMethod("paypal");

            $item = new PayPal\Api\Item();
            $item->setName('Video-'.$post->post_title)
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku($post->ID)
                ->setPrice($video_price);

            $itemList = new PayPal\Api\ItemList();
            $itemList->setItems(array($item));

            $amount = new PayPal\Api\Amount();
            $amount->setCurrency("USD")
                ->setTotal(intval($video_price));

            $transaction = new PayPal\Api\Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setInvoiceNumber(uniqid());

            $baseUrl = get_permalink($post->ID);
            $redirectUrls = new PayPal\Api\RedirectUrls();
            $successUrl = strpos($baseUrl, '?') !== false ? "$baseUrl&paypal_success=true" : "$baseUrl?paypal_success=true";
            $errorUrl = strpos($baseUrl, '?') !== false ? "$baseUrl&paypal_success=false" : "$baseUrl?paypal_success=false";
            $redirectUrls->setReturnUrl($successUrl)
                ->setCancelUrl($errorUrl);

            $payment = new PayPal\Api\Payment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            $payment->create($apiContext);
            $approvalUrl = $payment->getApprovalLink();
            wp_redirect($approvalUrl);
            exit(0);
        }

    }
}
add_action('init', 'vptf_paypal_processing');

function vptf_paypal_postprocessing(){

    $current_user = wp_get_current_user();

    if(isset($_GET['paypal_success']) && $current_user->ID){
        if($_GET['paypal_success'] == 'true'){


            //create api context
            require __DIR__ . '/paypal_bootstrap.php';

            $paymentId = $_GET['paymentId'];
            $payment = PayPal\Api\Payment::get($paymentId, $apiContext);
            $post = get_post(intval($payment->getTransactions()[0]->getItemList()->getItems()[0]->getSku()));

            $execution = new PayPal\Api\PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);

            $result = $payment->execute($execution, $apiContext);

            add_post_meta($post->ID, 'payed_users', $current_user->ID);

        } else {
            
            $GLOBALS['vptf_paypal_error'] = "PayPal Error!";

        }

    }
}
add_action('init', 'vptf_paypal_postprocessing');

//add menu
register_nav_menus(array(
    "footer-menu" => __('Footer Menu')
));

?>
