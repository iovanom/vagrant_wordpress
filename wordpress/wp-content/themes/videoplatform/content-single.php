<?php while(have_posts()): the_post();?>

<?php
//get post status
$post_status = get_post_meta($post->ID, 'vptf_status', true);

if($post_status && $post_status === 'ok'):

    $post_att = get_children(array('post_parent'=>$post->ID, 'post_type'=>'attachment', 'numberposts'=>1));
    foreach ($post_att as $v) {
        $post_video = $v;
        break;
    }

    //current user
    $current_user = wp_get_current_user();
    
    

    $video_price = get_post_meta($post->ID, 'vptf_price', true);
    if($video_price){
        $payed_users = get_post_meta($post->ID, 'payed_users');
    }
    $author_id = get_the_author_meta('ID');

    $post_thumbs = json_decode(get_post_meta($post->ID, 'thumbs', true));
//var_dump($post);
//{var_dump($post_video);
?>
<div id="content-single">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div id="content-video">
<?php if($author_id !== $current_user->ID && ($video_price && (!$payed_users || !in_array($current_user->ID, $payed_users)))): ?> 
<?php $views = vptf_video_viewed($post->ID, get_the_author_meta('ID'), false);?>

<div class="buy-video" style="text-align: center; padding-top: 200px; height: 480px; background-image: url(<?php echo get_home_url() . $post_thumbs[0];?>);">
<?php if($current_user->ID): ?>
<form action="/" method="post">
    <input type="hidden" name="paypal_processing" value="true">
    <input type="hidden" name="payed_post_id" value="<?php echo $post->ID; ?>">
    <input type="hidden" name="payed_user_id" value="<?php echo $current_user->ID; ?>">
    <button class="btn btn-lg btn-primary">$ <?php echo $video_price;?> - Buy now</button>
</form>
<?php else:?>
        <a href="<?= wp_login_url(get_permalink($post->ID)); ?>" class="btn btn-lg btn-primary">$ <?php echo $video_price;?> - Buy now</a>
<?php endif; ?>
    </div>

<?php else: ?>
<?php $views = vptf_video_viewed($post->ID, get_the_author_meta('ID')); ?>
        <video width="847" height="480" poster="<?php echo $post_thumbs[0]; ?>" controls>
             <source src="<?php echo $post_video->guid;?>" type="video/mp4">
             Your browser does not support the video tag.
        </video> 
<?php endif; ?>
    </div>
    <div id="content-title" class="panel panel-default">
        <div class="panel-body">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            <div class="col-sm-8 author-video">
                <?php echo get_avatar(get_the_author_meta('ID'), 45);?>
                <a class="author-name" href="<?= get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta('user_nicename'))?>">
                    <?php the_author(); ?>
                </a>
                <a class="btn btn-default" href="">Subscribe</a>
            </div>
            <div class="col-sm-4">
            <span class="badge pull-right video-views">Views  <?php echo $views;?></span>
            </div>
            <div class="clearfix"></div>
            <hr/>
            <div class="video-actions">
                <div class="col-sm-8">
                    <a class="btn btn-primary" href="#"><span class="glyphicon glyphicon-share-alt"></span>  Share</a>

                </div>
                <div class="col-sm-4">
                    <div class="pull-right" class="video-likes">

                        <a class="btn btn-success" id="video-like" href="#"><span class="glyphicon glyphicon-thumbs-up"></span>  12,253</a>   
                        <a class="btn btn-danger" id="video-no-like" href="#"><span class="glyphicon glyphicon-thumbs-down"></span>  575</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

	<div id="single-content" class="panel panel-default">
        <div class="panel-body">
            <div class="published-date">
                <strong>Published on <?php echo $post->post_date; ?></strong>

            </div>

		<?php
			the_content();
		?>

        </div>
	</div>

	<footer class="single-footer">
    </footer>
</article><!-- #post-## -->
</div>
<?php else: ?>
<h3>Video in processing...</h3>
<?php endif; ?>

<?php endwhile;?>

