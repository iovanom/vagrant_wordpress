<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> <?php bloginfo( 'name' ); ?> </title>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?> >
        <?php $navbar_top_pading = is_admin_bar_showing() ? 32 : 0; ?>
    <nav class="navbar navbar-fixed-top navbar-default" style="margin-top: <?= $navbar_top_pading ?>px">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= esc_url( home_url('/') ) ?>">
                    <?php if ( get_theme_mod( 'vptf_logo' ) ): ?>
                        <img src="<?= esc_url( get_theme_mod( 'vptf_logo' ) ) ?>" alt="" height="40">
                    <?php else: ?>
                        <?php bloginfo( 'name' ); ?>
                    <?php endif; ?>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <div class="row">
                    <div class="col-lg-7 col-lg-offset-3">
                        <form class="navbar-form navbar-left" role="search" method="get" id="searchform" action="<?php  echo esc_url( home_url( '/' ) ); ?>">
                            <div class="form-group">
                                <input class="form-control" name="s" placeholder="Search" type="text">
                            </div>
                            <button type="submit" class="btn btn-default">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                        <li><a class="btn btn-default" href="<?= is_user_logged_in() ? get_page_link(4) : wp_login_url('/'); ?>"><span class="glyphicon glyphicon-upload" ></span>  Upload</a></li>
<?php if ( !is_user_logged_in() ): ?>
                            <li><a class="btn btn-default" href="<?= wp_login_url('/'); ?>"><span class="glyphicon glyphicon-log-in"></span>  Sign-in</a></li>
                            <li><a class="btn btn-default" href="<?= wp_registration_url(); ?>"><span class="glyphicon glyphicon-open-file"></span>  Sign-up</a></li>
<?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

