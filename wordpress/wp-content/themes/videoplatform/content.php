<?php $post_count = 0;?>
<?php if(have_posts()): while(have_posts()): the_post();?>

<?php
//get post status
$post_status = get_post_meta($post->ID, 'vptf_status', true);
if($post_status && $post_status === 'ok'):
    $post_thumbs = json_decode(get_post_meta($post->ID, 'thumbs', true));
    $post_count++;      
//var_dump($post);
?>
<div class="col-sm-3 video-box">
    <div class="thumbnail">
<span class="video-price">
<?php 
$video_price = get_post_meta($post->ID, 'vptf_price', true);
if($video_price){
    echo '$ '. $video_price;
} else {
    echo 'Free';
}
?>
</span>
        <a href="<?= get_permalink($post) ?>">
        <img src="<?= $post_thumbs[0] ?>" data-thumbs='<?= get_post_meta($post->ID, "thumbs", true)?>'></img>
        </a>
        <h5>
            <a href="<?= get_permalink($post) ?>">
<?php
if(strlen($post->post_title) < 30){
    echo $post->post_title;
} else {
    echo substr($post->post_title, 0, 30) . '...';
}

?>
            </a>
        </h5>
        <div class="video-info">
            <div class="author-video">
                <span> Uploaded by:</span>
                <a href="<?= get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta('user_nicename'))?>">
                    <?php the_author(); ?>
                </a>
            </div>
            <div class="video-date">
                <span><?= $post->post_date ?> </span>
            </div>
            <div class="video-views">
                <span>
<?php echo get_post_meta($post->ID, 'views', true) ? get_post_meta($post->ID, 'views', true) : 0 ?> views
                </span>
            </div>
        </div>
    </div>
    
</div>
<?php if(($post_count % 4) == 0): ?>

<div class="clearfix"></div>

<?php endif; ?>
<?php endif; ?>
<?php endwhile; endif; ?>
<div class="clearfix"></div>
<div>
<?php echo paginate_links(); ?>
</div>

