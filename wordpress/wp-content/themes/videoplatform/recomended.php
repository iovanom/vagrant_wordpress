<?php

$recommended = new WP_Query(array(
    "tag"=>'Recommended', 
    "showposts"=>4
));

?>

<?php if($recommended->have_posts()): ?>

<h2> Recommended </h2>
<div><a class="pull-right" href="<?php echo get_tag_link(3);?>">All Recommended</a></div>
<hr/>

<?php endif; ?>

<?php if($recommended->have_posts()): while($recommended->have_posts()): $recommended->the_post(); ?>
<?php
//get post status
$post_status = get_post_meta($post->ID, 'vptf_status', true);
if($post_status && $post_status === 'ok'):
    $post_thumbs = json_decode(get_post_meta($post->ID, 'thumbs', true));
//var_dump($post);
?>
<div class="col-sm-3 video-box">
    <div class="thumbnail">
<span class="video-price">
<?php 
$video_price = get_post_meta($post->ID, 'vptf_price', true);
if($video_price){
    echo '$ '. $video_price;
} else {
    echo 'Free';
}
?>
</span>
        <a href="<?= get_permalink($post) ?>">
        <img src="<?= $post_thumbs[0] ?>" data-thumbs='<?= get_post_meta($post->ID, "thumbs", true)?>'></img>
        </a>
        <h5>
            <a href="<?= get_permalink($post) ?>">
<?php
if(strlen($post->post_title) < 30){
    echo $post->post_title;
} else {
    echo substr($post->post_title, 0, 30) . '...';
}

?>
            </a>
        </h5>
        <div class="video-info">
            <div class="author-video">
                <span> Uploaded by:</span>
                <a href="<?= get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta('user_nicename'))?>">
                    <?php the_author(); ?>
                </a>
            </div>
            <div class="video-date">
                <span><?= $post->post_date ?> </span>
            </div>
            <div class="video-views">
                <span>
<?php echo get_post_meta($post->ID, 'views', true) ? get_post_meta($post->ID, 'views', true) : 0 ?> views
                </span>
            </div>
        </div>
    </div>
    
</div>
<?php endif; ?>
<?php endwhile; endif; ?>
<div class="clearfix"></div>
