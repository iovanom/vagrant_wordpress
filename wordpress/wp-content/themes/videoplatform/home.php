<?php get_header(); ?>
<div class="container-fluid" style="margin-top: <?= 75 ?>px">
    <div class="row">
        <div class="col-md-2">
        <?php get_sidebar(); ?>
        </div>
        <div class="col-md-10" id="index-content">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="vptf-recomended">
                            <?php get_template_part('recomended'); ?>
                        </div>
                        <div id="vptf-recent">
                            <h2> Most Recent </h2>
                            <hr/>
                            <?php get_template_part('content');?>
                        </div>
                    </div>
            </div>

        </div>
    </div>
<?php get_footer();?>

