<?php get_header(); ?>
<div class="container-fluid" style="margin-top: <?= 75 ?>px" id="page">
    <div class="row">
        <div class="col-md-2">
        <?php get_sidebar(); ?>
        </div>
        <div class="col-md-10">
<?php if ( have_posts() ): while( have_posts() ): the_post(); ?>
        <div class="panel panel-default">
            <div class="panel-heading"><h1><?php echo the_title();?></h1></div>
            <div class="panel-body">
            
        <?php echo the_content(); ?>
            </div>
        </div>

    <?php endwhile; ?>
<?php endif; ?>

        </div>
    </div>
<?php get_footer();?>
