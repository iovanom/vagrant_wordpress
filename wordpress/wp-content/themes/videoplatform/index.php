<?php get_header(); ?>
<div class="container-fluid" style="margin-top: <?= 75 ?>px">
    <div class="row">
        <div class="col-md-2">
        <?php get_sidebar(); ?>
        </div>
        <div class="col-md-10" id="index-content">
                <div class="panel panel-default">
                    <div class="panel-body">

<h2>
<?php

if(is_home()){
    echo "Most Recent";
} else if(is_archive()){
    echo get_the_archive_title();
} else if( is_search()){
    echo "Search: " . get_search_query();
}

?>

</h2>
<hr/>
                        <?php get_template_part('content');?>
                    </div>
            </div>

        </div>
    </div>
<?php get_footer();?>

