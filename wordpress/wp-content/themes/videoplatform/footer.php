        <div id="footer">
                <div class="panel panel-default">
                    <div class="panel-body">
<div class="col-md-2">
    <span class="vptf-copy"><span class="glyphicon glyphicon-copyright-mark"></span>  <?php echo date('Y');?>  <?php  bloginfo( 'name' ); ?></span> 
</div>
<div class="col-md-10">
<?php 

wp_nav_menu(array(

    "container" => "ul",
    "menu_class" => "list-inline pull-right",
    "depth" => 1,
    "theme_location" => "footer-menu"

));


?>
<!--    <ul class="list-inline pull-right">
        <li><a href="#">Contect Us</a></li>
        <li><a href="#">Terms of Services</a></li>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Copyright</a></li>
    </ul>
-->
</div>

                    
                    </div>
                </div>
        </div>
    </div> <!-- .container -->
    <?php wp_footer(); ?>
</body>
</html>
