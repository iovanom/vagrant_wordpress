<?php get_header(); ?>
<div class="container" style="margin-top: <?= 75 ?>px">
    <div class="row">
        <div class="col-md-3">
        <?php get_sidebar(); ?>
        </div>
        <div class="col-md-9" id="single-main-content">
                <?php get_template_part('content-single');?>
<?php if( comments_open() || get_comments_number() ):
    comments_template();
endif;
?>
        </div>
    </div>
<?php get_footer();?>

